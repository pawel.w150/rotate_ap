import easygui as gui
from PyPDF2 import PdfReader, PdfWriter

input_path = gui.fileopenbox(
    title = "Select a PDF to rotate...",
    default = "*.pdf"
    )

if input_path is None:
    exit()

# Let the user select one of `90`, `180` or `270` degrees to rotate
# the PDF pages.
choices = ("90", "180", "270")
degrees = gui.buttonbox(
    msg = "Rotate the PDF clockwise by how many degrees",
    title = "Choose rotation...",
    choices = choices,
)

# converting values to intiger
degrees = int(degrees)

# display a file selection dialog for saving the rotated PDF
save_title = "Save the rotated PDF as ..."
file_type = "*.pdf"
output_path = gui.filesavebox(title=save_title, default=file_type)

# if the user tries to save with the same name as the input file:
# alert the user with a message box that this is not allowed
while input_path == output_path:
    gui.msgbox(msg = "Cannot overwrite original file!")
    output_path = gui.filesavebox(title=save_title, default=file_type)

# if the user cancels the file save dialog, then exit the program.
if output_path is None:
    exit()

# perform the page rotation - open the selected PDF
input_file = PdfReader(input_path)
output_pdf = PdfWriter()

# rotate all of the pages
for page in input_file.pages:
    page = page.rotate(degrees)
    output_pdf.add_page(page)

# save the rotated PDF to the seleced file
with open(output_path, "wb") as output_file:
    output_pdf.write(output_file)
    
